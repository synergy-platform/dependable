<?php

namespace Synergy\Dependable;

/**
 * Part of the Dependable package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Dependable
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/dependable
 */

interface Dependable {

	/**
	 * Returns the unique slug for the item
	 *
	 * @return mixed
	 */
	public function getSlug();

	/**
	 * Returns an array of the item's dependencies
	 *
	 * @return mixed
	 */
	public function getDependencies();
}